<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body bgcolor="#80D0E0">
<div align="center">

<h1>Przypisz studentów do zajęć</h1>

<form action="servlet" method="POST">
<table>
    <tr>
        <th>Student</th>
        <th>Zajęcia</th>
    </tr>
    <tr>
        <td>
            <select name="studentId" multiple="multiple">
                <c:forEach var="student" items="${students}">
                    <option value="${student.id}">
                        ${student.firstName} ${student.lastName}
                    </option>
                </c:forEach>
            </select>

        </td>
        <td>
            <select name="courseId">
                <c:forEach var="course" items="${courses}">
                    <option value="${course.id}">
                        ${course.name} (${course.startDate} - ${course.startDate})
                    </option>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" value="Zapisz">
            <input type="hidden" name="command" value="AssignStudents">
        </td>
    </tr>
</table>

</form>

</div>
<%@ include file="go_to_home.jspf" %>
</body>
