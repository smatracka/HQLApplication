<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body bgcolor="#80D0E0">
<div align="center">

<h1>Nowe zajęcia</h1>

<form action="servlet" method="POST">
<table>
    <tr>
        <td>Nazwa:</td>
        <td><input type="text" name="name"></td>
    </tr>
    <tr>
        <td>Data rozpoczęcia:</td>
        <td><input type="text" name="startDate"> (rrrr-mm-dd)</td>
    </tr>
    <tr>
        <td>Data zakończenia:</td>
        <td><input type="text" name="endDate"> (rrrr-mm-dd)</td>
    </tr>
    <tr>
        <td>Nauczyciel:</td>
        <td>
            <select name="teacherId">
                <c:forEach var="teacher" items="${teachers}">
                    <option value="${teacher.id}">
                        ${teacher.firstName} ${teacher.lastName}
                    </option>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" value="Zapisz">
            <input type="hidden" name="command" value="NewCourse">
        </td>
    </tr>
</table>
</form>

</div>
<%@ include file="go_to_home.jspf" %>
</body>
