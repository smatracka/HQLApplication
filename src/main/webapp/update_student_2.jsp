<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body bgcolor="#80D0E0">
<div align="center">

<h1>Modyfikuj dane studenta</h1>

<form action="servlet" method="POST">
<table>
    <tr>
        <td>Nr indeksu:</td>
        <td><input type="text" name="bookNumber" value="${student.bookNumber}"></td>
    </tr>
    <tr>
        <td>Imię:</td>
        <td><input type="text" name="firstName" value="${student.firstName}"></td>
    </tr>
    <tr>
        <td>Nazwisko:</td>
        <td><input type="text" name="lastName" value="${student.lastName}"></td>
    </tr>
    <tr>
        <td>Data urodzenia:</td>
        <td><input type="text" name="birthDate" value="${student.birthDate}"> (rrrr-mm-dd)</td>
    </tr>
    <tr>
        <td>Płeć:</td>
        <td>
            <c:set var="k_checked" value="" />
            <c:set var="m_checked" value="" />
            <c:if test="${!student.man}">
                <c:set var="k_checked" value="checked=\"checked\"" />
            </c:if>
            <c:if test="${student.man}">
                <c:set var="m_checked" value="checked=\"checked\"" />
            </c:if>
            <input type="radio" name="man" value="K" ${k_checked}> kobieta<br>
            <input type="radio" name="man" value="M" ${m_checked}> mężczyzna
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" value="Zapisz">
            <input type="hidden" name="command" value="UpdateStudent2">
            <input type="hidden" name="id" value="${student.id}">
        </td>
    </tr>
</table>
</form>

</div>
<%@ include file="go_to_home.jspf" %>
</body>
