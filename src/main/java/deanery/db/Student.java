package deanery.db;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
public class Student {

	@Id
	@GeneratedValue
	private Integer id;
	@Column(name = "ST_BOOKNUM")
	private Integer bookNumber;
	@Column(name = "ST_FIRSTNAME", length = 45)
	private String firstName;
	@Column(name = "ST_LASTNAME", length = 45)
	private String lastName;
	@Temporal(TemporalType.DATE)
	@Column(name = "ST_BIRTH")
	private Date birthDate;
	@Column(name = "ST_MAN")
	private Boolean man;
	@ManyToMany
	private Collection<Course> courses;


	
	public String getSexIndicator() {
		if (Boolean.TRUE.equals(getMan())) {
			return "M";
		} else {
			return "K";
		}
	}

	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getBookNumber() {
		return bookNumber;
	}
	public void setBookNumber(Integer bookNumber) {
		this.bookNumber = bookNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getMan() {
		return man;
	}
	public void setMan(Boolean man) {
		this.man = man;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Student student = (Student) o;
		return Objects.equals(id, student.id);
	}

	@Override
	public int hashCode() {

		return this.id*10;
	}

    public Collection<Course> getCourses() {
        return courses;
    }
}
