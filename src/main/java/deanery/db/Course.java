package deanery.db;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
public class Course {

    @Id
    @GeneratedValue
    private Integer teacherId;
    @Column(name = "COURSE_NAME")
    private String name;
    @Column(name = "COURSE_STARTDATE")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "COURSE_ENDDATE" )
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @ManyToOne
    private Teacher teacher;

    @ManyToMany(mappedBy = "courses")
    private Collection<Student> students;

    public Integer getTeacherId() {
        return teacherId;
    }
    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public Teacher getTeacher() {
        return teacher;
    }
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public int hashCode() {
        return this.teacherId*10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(teacherId, course.teacherId);
    }

    public Collection<Student> getStudents() {
        return students;
    }
}
