package deanery.db;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Teacher {

	@Id
	@GeneratedValue
	@Column(name = "tcId")
	private Integer id;
	@Column(name = "tcName", length = 45)
	private String firstName;
	@Column(name = "tcSurname", length = 45)
	private String lastName;
	@Temporal(TemporalType.DATE)
	@Column(name = "tcBirth")
	private Date birthDate;
	@Column(name = "tcMan")
	private Boolean man;
	@OneToMany(mappedBy = "teacher")
	private Collection<Course> courses;
	
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getMan() {
		return man;
	}
	public void setMan(Boolean man) {
		this.man = man;
	}
	
}
