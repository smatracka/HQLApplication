package deanery.command;

import deanery.db.Teacher;

import java.util.ArrayList;
import java.util.List;

public class FindAllTeachers extends AbstractCommand {

	private List<Teacher> teachers;

	public void execute() {
		teachers = new ArrayList<Teacher>(session.createQuery("select * from Teacher").list());
	}

	public List getTeachers() {
		return teachers;
	}
	
}
