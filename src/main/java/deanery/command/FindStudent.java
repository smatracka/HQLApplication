package deanery.command;

import deanery.db.Student;

import javax.management.Query;


public class FindStudent extends AbstractCommand {
	private Integer id = null;
	private Student student;
	private Query query;

	public void setId(Integer id) {
		this.id = id;
	}

	public void execute() {
		this.student = (Student) session.get(Student.class, this.id);
	}

	public Student getStudent() {
		org.hibernate.Query query = session.createQuery("from Student s where s id = id");
		return student;
	}

}
