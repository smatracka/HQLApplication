package deanery.command;

import deanery.db.Course;

import java.util.ArrayList;
import java.util.List;

public class FindAllCourses extends AbstractCommand {

	private List<Course> courses;

	public void execute() {
		courses = new ArrayList<>(session.createQuery("select * from Courses").list());
	}

	public List getCourses() {
		return courses;
	}
	
}
