package deanery.command;

import deanery.db.Course;

import java.util.Date;

public class NewCourse extends AbstractCommand {

	Course course = new Course();

	public void setEndDate(Date endDate) {
		course.setEndDate(endDate);
	}
	public void setName(String name) {
		course.setName(name);
	}
	public void setStartDate(Date startDate) {
		course.setStartDate(startDate);
	}
	public void setTeacherId(Integer id) {
		course.setTeacherId(id);
	}

	public void execute() {
		session.save(course);
	}

}
