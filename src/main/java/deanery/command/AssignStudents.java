package deanery.command;


import deanery.db.Course;
import deanery.db.Student;

import java.util.Collection;


public class AssignStudents extends AbstractCommand {

	private Integer[] studentIds;
	private Integer courseId;

	public void execute() {
		Course course = (Course) session.load(Course.class, courseId);
		Collection<Student> students = course.getStudents();
		for (int i = 0; i < studentIds.length; i++){
			Student student = (Student) session.load(Student.class, studentIds[i]);
			students.add(student);
		}
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public void setStudentIds(Integer[] studentIds) {
		this.studentIds = studentIds;
	}
}
