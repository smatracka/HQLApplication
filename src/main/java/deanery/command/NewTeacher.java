package deanery.command;

import deanery.db.Student;
import deanery.db.Teacher;

import java.util.Date;

public class NewTeacher extends AbstractCommand {
	Teacher teacher = new Teacher();

	public void setBirthDate(Date birthDate) {
		teacher.setBirthDate(birthDate);
	}
	public void setFirstName(String firstName) {
		teacher.setFirstName(firstName);
	}
	public void setLastName(String lastName) {
		teacher.setLastName(lastName);
	}
	public void setMan(Boolean man) {
		teacher.setMan(man);
	}

	public void execute() {
		session.save(teacher);
	}

}
