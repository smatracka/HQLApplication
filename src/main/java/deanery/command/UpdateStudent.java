package deanery.command;

import deanery.db.Student;

import java.util.Date;

public class UpdateStudent extends AbstractCommand {

	Student student = new Student();

	public void setId(Integer id) {
		student = (Student) session.load(Student.class, id);
	}
	public void setBirthDate(Date birthDate) {
	student.setBirthDate(birthDate);
	}
	public void setBookNumber(Integer bookNumber) {
		student.setBookNumber(bookNumber);
	}
	public void setFirstName(String firstName) {
		student.setFirstName(firstName);
	}
	public void setLastName(String lastName) {
		student.setLastName(lastName);
	}
	public void setMan(Boolean man) {
		student.setMan(man);
	}

	public void execute() {
		session.update(student);
	}

}
