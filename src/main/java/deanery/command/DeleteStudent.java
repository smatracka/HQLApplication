package deanery.command;


import deanery.db.Course;
import deanery.db.Student;
import java.util.Iterator;

public class DeleteStudent extends AbstractCommand {

	private Integer id;

	public void setId(Integer id) {
		this.id = id;
	}

	public void execute() {
		Student student = (Student) session.load(Student.class, id);
		Iterator iterator = student.getCourses().iterator();
		while (iterator.hasNext()){
			Course course = (Course) iterator.next();
			course.getStudents();
		}
		session.delete(student);
	}

}
