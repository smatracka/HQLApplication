package deanery.command;

import deanery.db.Student;

import java.util.ArrayList;
import java.util.List;

public class FindAllStudents extends AbstractCommand {

	private List<Student> students;

	public void execute() {
		students = new ArrayList<Student>(session.createQuery("select * from Student").list());
	}

	public List getStudents() {
		return students;
	}
	
}
