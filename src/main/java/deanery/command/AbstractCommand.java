package deanery.command;

import org.hibernate.Session;

public abstract class AbstractCommand {

	protected Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	public abstract void execute();
	
}
